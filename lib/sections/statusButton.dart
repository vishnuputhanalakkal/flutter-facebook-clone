import 'package:flutter/material.dart';

class StatusBtn extends StatelessWidget {
  Widget headerButton({
    required String buttonText,
    required IconData buttonIcon,
    required void Function() buttonAction,
    required Color color,
  }) {
    return TextButton.icon(
        onPressed: buttonAction,
        icon: Icon(
          buttonIcon,
          color: color,
        ),
        label: Text(
          buttonText,
          style: TextStyle(color: Colors.black),
        ));
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      height: 40,
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
        children: [
          headerButton(
              buttonText: "Live",
              buttonIcon: Icons.video_call,
              buttonAction: () {},
              color: Colors.red),
          VerticalDivider(
            thickness: 1,
            color: Colors.grey[400],
          ),
          headerButton(
              buttonText: "Photo",
              buttonIcon: Icons.image,
              buttonAction: () {},
              color: Colors.green),
          VerticalDivider(
            thickness: 1,
            color: Colors.grey[400],
          ),
          headerButton(
              buttonText: "Room",
              buttonIcon: Icons.video_call,
              buttonAction: () {},
              color: Colors.purple),
        ],
      ),
    );
  }
}
