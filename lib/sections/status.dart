import 'package:flutter/material.dart';
import 'package:flutter_facebook/imageConsts.dart';

class Status extends StatelessWidget {
  const Status({super.key});

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.only(top: 8),
      child: ListTile(
        leading: ClipRRect(
          borderRadius: BorderRadius.circular(100),
          child: Image.network(bryan, height: 50,width: 50,),
        ),
        title: TextField(
          decoration: InputDecoration(
            hintText: "Share what's on you mind",
            focusedBorder: InputBorder.none,
            enabledBorder: InputBorder.none,
            disabledBorder: InputBorder.none
          ),
        ),
      ),
    );
  }
}
