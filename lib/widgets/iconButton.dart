import 'package:flutter/material.dart';

class CustomIconBtn extends StatelessWidget {
  final Widget icon;
  void Function() action;

  CustomIconBtn({required this.icon, required this.action});

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.all(5),
      decoration: BoxDecoration(
        color: Colors.grey[200],
        shape: BoxShape.circle,
      ),
      child: IconButton(
        iconSize: 25,
        icon: icon,
        color: Colors.grey[600],
        onPressed: action,
      ),
    );
  }
}
