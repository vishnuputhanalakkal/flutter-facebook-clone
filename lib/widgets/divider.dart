import 'dart:ffi';

import 'package:flutter/material.dart';

class CustomDivider extends StatelessWidget {
  final double thickness;

  CustomDivider({required this.thickness});

  @override
  Widget build(BuildContext context) {
    return Divider(
      thickness: thickness,
      color: Colors.grey[500],
    );
  }
}
