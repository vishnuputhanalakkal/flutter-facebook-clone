import 'package:flutter/material.dart';
import 'package:flutter_facebook/home.dart';
import 'package:flutter_facebook/widgets/iconButton.dart';

class App extends StatelessWidget {
  const App({super.key});

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Scaffold(
        appBar: AppBar(
          title: Text(
            "facebook",
            style: TextStyle(
                color: Colors.blue, fontWeight: FontWeight.bold, fontSize: 25),
          ),
          backgroundColor: Colors.white,
          elevation: 0,
          actions: [
            CustomIconBtn(
                icon: Icon(Icons.search),
                action: () {
                  print("search button clicked");
                }),
            CustomIconBtn(
                icon: Icon(Icons.message),
                action: () {
                  print("message button clicked");
                }),
          ],
        ),
        body: Home(),
      ),
    );
  }
}
