import 'package:flutter/material.dart';
import 'package:flutter_facebook/sections/status.dart';
import 'package:flutter_facebook/sections/statusButton.dart';
import 'package:flutter_facebook/widgets/divider.dart';

class Home extends StatelessWidget {

  @override
  Widget build(BuildContext context) {
    return Container(
      width: MediaQuery.of(context).size.width,
      height: MediaQuery.of(context).size.height,
      child: ListView(
        children: [
          Status(),
          CustomDivider(thickness: 1),
          StatusBtn(),
          CustomDivider(thickness: 8)
        ],
      ),
    );
  }
}